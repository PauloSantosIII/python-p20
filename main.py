class Sorcerer:
    def __init__(self, name: str, age: int):
        self.name = name
        self.age = age
        self.health = 100
        self.spell_list = []

    def learn_spell(self, spell: str):
        self.spell_list.append(spell)

    def kind():
        return f"I am a <{self.__class__.__name__}>"

    def __len__(self):
        return self.health
          
    def __str__(self):
        spells = sum(self.spell_list)
        representation = "-- Sorcerer -- {} | Spells Amount : {}".format(self.health, self.spell_list)

        return representation

    def __repr__(self):
        return f"<{self.__class__.__name__}>"

class Professor(Sorcerer):
    def __init__(self, name: str, age: int):
        super().__init__(name, age)

    def teach(self):
        self.is_teaching = True

    def stop_teaching(self):
        self.is_teaching = False

    def kind():
        return f"I am a <{self.__class__.__name__}>"

class Student(Sorcerer):
    def __init__(self, name: str, age: int, academy_year: int, house_name: str):
        super().__init__(name, age)
        self.academy_year = academy_year
        self.house_name = house_name
        self.class_list = []
    
    def add_class(self, class_name):
        self.class_list.append(class_name)
        return self.class_list
    
    def attend_class(self):
        self.is_attending = True
    
    def stop_attending_class(self):
        self.is_attending = False

    def kind():
        return f"I am a <{self.__class__.__name__}>"